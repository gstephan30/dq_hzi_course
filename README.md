# HZI Digital Epidemiology Course I: Data Quality

---

## Table of Content
* [Course Description](#desc)
* [Day 1 : Data Quality & Data Browsing](#day1)
* [Day 2 : Data Completeness](#day2)
* [Day 3 : Data Accuracy](#day3)
* [Day 4 : Data Consistency](#day3)
* [Day 5 : Data Quality Indicators](#day5)
* [Day 6 : Data Visualization/Reporting](#day6)
* [Final feedback form](#feedback)


## <a name="desc"></a>Description
This course is part one of three [HiGHmed](http://highmed.org/) teaching modules. HiGHmed is a research project, developing digital tools for hospitals to ultimately improve patient care. HiGHmed stands for Heidelberg, Gottingen, and Hannover Medical Informatics.

The HZI participates in the HiGHmed Teaching Group and UseCase Infection Control, developing algorithms for cluster and outbreak detection. This module is developed within the HiGHmed Teaching Group and is intended to be a present course and also an __online module__ for students interested in that topic, hence the module is open source and can be started on individual bases.

This module is part one of a series of three modules. First part will be about the __evaluation of data quality__ in clinical/epidemiological research. This module will be commenced in 2018. Second module will be initiated in 2019 about __statistical learning__ in clinical research. Third module will be about __signal detection__ starting in 2020, which is related to the use case the HZI is contributing within HiGHmed (see [Table 1](#table1)).

<a name="table1"></a>*Table 1: Structure HiGHmed Digital Epidemiology Courses*

Course                        | Planned Date of first present meeting
:---------------------------: | :------------------------------------:
Evaluation of Data Quality    | Jan 2019
Statistical Learning          | Oct 2019
Signal Detection              | Oct 2020


This course in following the teaching framework of [Ten quick tips for teaching programming](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006023); Brown & Wilson (2018).



## Teaching Units (TU)
Total: 45min == 1 TU; 6h per day a 6 days == 8*6TU = 48 TU

# Preparing for the course
To prepare for the course it is __required__ to install R with the `tidyverse` on your computer.

Second, more __optional__, a list of good references is provided below. Best paper would be to start with Weiskopf & Weng (2013), but you can also search on [Pubmed](https://www.ncbi.nlm.nih.gov/pubmed) or [Google Scholar](scholar.google.com) for any article related to your field of research in combination with the terms: “evaluation of data quality”, “data quality”, “data completeness”, “data monitoring” or “data cleaning”. Any prior knowledge will improve the discussions.

To complete the list, links to free online tutorials and communities are also listed below.


## Install on your computer
[R Version 3.5 or higher](https://cran.r-project.org/)

Recommended IDE: [RStudio](https://www.rstudio.com/products/rstudio/download/#download)

In R please enter the following commands to install the important libraries:
`install.packages('tidyverse')`

For detailed instructions, please follow this [link](http://stat545.com/block000_r-rstudio-install.html). 

## Readings

### Data Quality
Book: [Exploratory Data Mining and Data Cleaning](https://onlinelibrary.wiley.com/doi/book/10.1002/0471448354); Dasu & Johnson (2003).

Paper: [Qualitätsstandards für epidemiologische Kohortenstudien](https://link.springer.com/article/10.1007/s00103-017-2658-y); Schmidt et al. (2018).

Book: [Datenqualität in der medizinischen Forschung](https://www.tmf-ev.de/News/articleType/ArticleView/articleId/234.aspx); Nonnemacher, Weiland & Stausberg (2014).

Paper: [Methods and dimensions of electronic health record data quality assessment: enabling reuse for clinical research](https://academic.oup.com/jamia/article/20/1/144/2909176); Weiskopf & Weng (2013).

Paper: [A Data Quality Assessment Guideline for Electronic Health Record Data Reuse](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5983018/); Weiskopf, Bakken, Hripcsak & Weng (2017).

Book: [Missing Data in Clinical Studies](https://www.wiley.com/en-us/Missing+Data+in+Clinical+Studies-p-9780470849811); Molenberghs & Kenward (2007).



### Data Analysis in R
(Online) Book: [R for Data Science](http://r4ds.had.co.nz/); Gorlemund & Wickham (2017).

## Recommended tutorials
[Manipulating Data in R](https://www.kaggle.com/rtatman/manipulating-data-with-the-tidyverse); Rachel Tatman Kaggle Tutorial (2018).

[Visualization of Data in R](https://www.kaggle.com/rtatman/visualizing-data-with-ggplot2); Rachel Tatman Kaggle Tutorial (2018).

## Communities
[r-bloggers](https://www.r-bloggers.com/): Community of bloggers posting topics about R





# <a name="day1"></a>Day 1 : Data Quality & Data Browsing

---

The course is planned as group work. Students will be split into 2-3 groups (~3 persons each group). In accordance to [Bloom's taxonomy](https://en.wikipedia.org/wiki/Bloom%27s_taxonomy), the student listen to short talks and watch the live coding of the instructor (__cognitive domain I: knowledge__). The `challenge of the day` is the same task for each group, while each group has to present the solution to the other groups at the end of the day (__cognitive domain II: comprehending__).

## Outline
TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Course Orientation (incl. Challenge of the Day)     | Talk
9.30am - 10.15am  | 45min    | Data Quality Assessment in Clinical Research        | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 11.00am | 30min    | Data Browsing: Data Tables                          | Live Coding
11.00am - 12.15pm | 75min    | Data Browsing: Data Tables                          | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.30pm      | 30min    | Data Visualization                                  | Live Coding
1.30pm - 3pm      | 90min    | Data Visualization                                  | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work


## Aim
*	Understand the impact of data quality for the outcome of clinical and epidemiological research
*	Understand the importance to evaluate data and to setup as early as possible processes to assess and improve data quality
*	Understand data tables
*	Understand the functions :
    +	`select()` - e.g. select only sex and age of your dataset,
    +	`filter()` - e.g. show only female persons in my dataset and
    +	`mutate()` - e.g. calculate the BMI, other units or define age groups
    +	`ggplot()` - e.g. histograms/density charts, dot-plots


## Challenge of the Day
e.g. identify characteristics of your dataset (or variable subset), describe problems and possible glitches

## Talk
Slides URL

## Supplementary Material
Dataset: [okcupid](https://github.com/rudeboybert/JSE_OkCupid) or Survey Data with much NA (tba)

## Readings
[Defining and Improving Data Quality in Medical Registries: A Literature Review, Case Study, and Generic Framework](https://academic.oup.com/jamia/article/9/6/600/1036696); Arts, de Keizer & Scheffer (2002).

[Measuring Data Quality: A Review of the Literature between 2005 and 2013](http://www.ncbi.nlm.nih.gov/pubmed/25991245); Stausberg, Nasseh, Nonnemacher (2015).


# <a name="day2"></a>Day 2 : Data Completeness

---

The second day will start with a repeat of the lessons learned on the day before. This is done with a small quiz with multiple choices. Each group will present and explain the results. The rest of the day is dedicated to the data quality dimension __data completeness__.

## Outline
TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Feedback/Quiz (incl. Challenge of the Day)          | Talk
9.30am - 10.15am  | 45min    | Data completeness                                   | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 10.45am | 15min    | Evaluation of Data completeness                     | Live Coding
10.45am - 12.15pm | 90min    | Evaluation of Data completeness                     | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.25pm      | 25min    | Summarise a Dataset                                 | Live Coding
1.25pm - 3pm      | 95min    | Summarise your Dataset                              | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work


## Aim
* Understand the principle of Data Completeness
* Distinguishing missing at random (MAR) from missing completely at random (MCAR)
* Able to detect missing data using e.g. `sum(is.na())` or the [VIM-package](https://cran.r-project.org/web/packages/VIM/index.html).
    + able to calculate missing in percent
    + able to visualize missing data in a plot
* data imputation e.g. with the [mice-package](https://cran.r-project.org/web/packages/mice/index.html), [tutorial]( https://www.kaggle.com/rtatman/data-cleaning-challenge-imputing-missing-values).


## Challenge of the Day
1. Summarise and visualise your dataset in regards to data completeness.
2. How could you improve the data?

## Talk
Slides URL

## Supplementary Material
Dataset: (e.g. Survey Data with much NA, focus on )

## Readings





# <a name="day3"></a>Day 3 : Data Accuracy

---

## Outline
TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Feedback/Quiz (incl. Challenge of the Day)          | Talk
9.30am - 10.15am  | 45min    | Data accuracy                                       | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 10.45am | 15min    | Evaluation of Data accuracy                         | Live Coding
10.45am - 12.15pm | 90min    | Evaluation of Data accuracy                         | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.25pm      | 25min    | Summarise a Dataset                                 | Live Coding
1.25pm - 3pm      | 95min    | Summarise your Dataset                              | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work


## Aim
* Understand Outliers
* Able to detect outliers using e.g. using the [outlier-package](https://cran.r-project.org/web/packages/outliers/) - [tutorial](https://www.kaggle.com/rtatman/data-cleaning-challenge-outliers) 
    + Able to calculate outliers
    + Able to visualize outliers data in a plot

## Challenge of the Day

## Talk
Slides URL

## Supplementary Material
Dataset: (e.g. Survey Data with much NA, focus on )




# <a name="day4"></a>Day 4 : Data Consistency

---

## Outline
TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Feedback/Quiz (incl. Challenge of the Day)          | Talk
9.30am - 10.15am  | 45min    | Data consistency                                    | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 10.45am | 15min    | Evaluation of Data consistency                      | Live Coding
10.45am - 12.15pm | 90min    | Evaluation of Data consistency                      | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.25pm      | 25min    | Summarise a Dataset                                 | Live Coding
1.25pm - 3pm      | 95min    | Summarise your Dataset                              | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work


## Aim
* Distinguish between accuracy and consistency
* Detect and report data contradictions and inadmissible values


## Challenge of the Day

## Talk
Slides URL

## Supplementary Material
Dataset: (e.g. Survey Data with much NA, focus on )

# <a name="day5"></a>Day 5 : Data Quality Indicators

---

## Outline
TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Quiz (incl. Challenge of the Day)                   | Talk
9.30am - 10.15am  | 45min    | Metrics to determine  the quality of data           | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 10.45am | 15min    | Data Quality Indicators                             | Live Coding
10.45am - 12.15pm | 90min    | Data Quality Indicators                             | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.25pm      | 25min    | Summarise a Dataset                                 | Live Coding
1.25pm - 3pm      | 95min    | Summarise your Dataset                              | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work



## Aim
* Understand data quality indicators
* Calculate selected TMF data quality indicators, e.g. data quality entered during the weekend


## Challenge of the Day

## Talk
Slides URL

## Supplementary Material
Dataset: (e.g. Survey Data with much NA, focus on )

# <a name="day6"></a>Day 6 : Data Visualization/Reporting

---

TUs: 6h == 8 TUs

Time              | Duration | Type                                                | Content
----------------- | -------- | --------------------------------------------------- | ---------------
9am - 9.30am      | 30min    | Quiz (incl. Challenge of the Day)                   | Talk
9.30am - 10.15am  | 45min    | Data Communication                                  | Talk
10.15am - 10.30am | 15min    | BREAK                                               | -
10.30am - 10.45am | 15min    | Data Visualization/Reporting                        | Live Coding
10.45am - 12.15pm | 90min    | Data Visualization/Reporting                        | Practical
12.15pm - 1pm     | 45min    | LUNCH BREAK                                         | -
1pm - 1.25pm      | 25min    | Summarise a Dataset                                 | Live Coding
1.25pm - 3pm      | 95min    | Summarise your Dataset                              | Practical
3pm - 3.15pm      | 15min    | BREAK                                               | -
3.15pm - 4.15pm   | 60min    | Group Result Presentation                           | Group Work

## Aim
* Fundamentals of reporting research outcomes
* Knowing how to avoid misleading precision ("Scheingenauigkeit")
* Knowing how to export data, e.g. for Stata, Excel, or HTML/pdf files.

## Challenge of the Day

## Talk
Slides URL

## Supplementary Material
Dataset: (e.g. Survey Data with much NA, focus on )


# <a name="feedback"></a>Final Feedback Form
[Feedback Form](https://goo.gl/forms/ttgl6CRQH0MQP0Nf1)


# Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
