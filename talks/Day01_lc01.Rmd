---
title: "Digital Epidemiology - Evaluation of Data Quality"
subtitle: "Day 1 - Data Browsing"
author: 
- "Stephan Glöckner"
- "email: stephan.gloeckner@helmholtz-hzi.de"
date: "Braunschweig, January 28th, 2019"
output:
  xaringan::moon_reader:
    nature:
      ratio: '14.6:9'
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      navigation:
        scroll: false
    css: ["default", "css/style.css"]
---
```{r include=FALSE}
source(here::here("talks/helper.R"))
library(RefManageR)
BibOptions(check.entries = FALSE, 
           bib.style = "authoryear", 
           style = "markdown",
           dashed = TRUE)
bib.file <- "lit03.bib"
bib <- ReadBib(bib.file)
```


class: inverse, center, middle

# Outline

<h2>1. Setup</h2> 

<h2>2. Data Transforming</h2>


---
# Setup

* R
* RStudio
* `install.packages(tidyverse)`

---
# Basics

## Objects & functions

* objects include some kind of information (e.g. a dataset or subset of data)
* `dataframes`/`tibbles` are 2D objects with data
* vectors are 1D objects with data

```{r}
head(mtcars)
```

---
# Basics

## Objects & functions

* functions are always followed by `()`, in the brackets are agruments

```{r}
sqrt(9)
```

* to get help about a function or object type e.g. `?median`
* best source of help:
    - formulate your problem with single keyterms
    - use `stackoverlow`, r-mailing lists or `r-bloggers` - help (there are also other sources, but these are the most frequent)

---
# Steps for Data Analysis

```{r out.width = '90%', echo=FALSE, fig.align='center'}
knitr::include_graphics("https://d33wubrfki0l68.cloudfront.net/571b056757d68e6df81a3e3853f54d3c76ad6efc/32d37/diagrams/data-science.png")
```

`r Citet(bib, "wickham2016r")`


---
# Loading Libraries, Import Data

```{r echo=FALSE}
knitr::opts_chunk$set(message = FALSE, warning = FALSE)
```


```{r}
library(tidyverse)
library(janitor)    # you need to install fist with: install.packages('janitor')
```



```{r }
df <- read_csv("../data/tidying/nyc_restaurants.csv")
head(df)
```

---
# Useful functions

```{r}
glimpse(df)
```

---
# Filter (rows/observations)

* [More examples](https://r4ds.had.co.nz/transform.html#filter-rows-with-filter)

```{r}
filter(df, cuisine_description == "Mexican")
```

---
# Select (colums/variables)

* [More examples](https://r4ds.had.co.nz/transform.html#select)

```{r}
select(df, cuisine_description, boro)
```

---
# Pipe

* a pipe in this context is a connection for several commands
* the pipe is `%>%` (Crtl + Shift + M)
* you can read `%>%` as "and than"

```{r}
df %>% 
  filter(cuisine_description == "Mexican") %>% 
  select(cuisine_description, boro) %>% 
  print(n = 4)
```


---
# How do I get:

```{r echo=FALSE}
df %>% 
  select(zipcode, violation_code, grade) %>% 
  filter(grade == "A")
```


---
# Mutate (create new column)


```{r}
df %>% select(inspection_date)
```

* [More examples](https://r4ds.had.co.nz/transform.html#add-new-variables-with-mutate)

---
# Mutate (create new column)

```{r}
library(lubridate)
df %>% select(inspection_date) %>% 
  mutate(inspection_date = mdy(inspection_date))
```

* `lubridate` powerful package for dates - [Link](https://lubridate.tidyverse.org/)

---
# arrange

```{r}
df %>% 
  arrange(desc(zipcode))
```


---
# group_by & summarise

```{r}
df %>% 
  mutate(inspection_date = mdy(inspection_date)) %>% 
  group_by(cuisine_description) %>% 
  summarise(first_inspection = min(inspection_date))
```

* [More examples](https://r4ds.had.co.nz/transform.html#grouped-summaries-with-summarise)

---
# count

```{r}
df %>% 
  count(cuisine_description, boro, sort = TRUE)
```


---
# distinct

```{r}
df %>% 
  add_count(boro) %>% 
  add_count(cuisine_description, boro) %>% 
  select(cuisine_description, boro, n, nn) %>% 
  distinct() %>% 
  arrange(desc(nn)) %>% 
  print(n = 4)
```

---
# What is the output of this script?

```{r eval=FALSE}
df %>% 
  add_count(boro) %>% 
  add_count(cuisine_description, boro) %>% 
  select(cuisine_description, boro, n, nn) %>% 
  distinct() %>% 
  arrange(desc(nn)) %>% 
  mutate(prozent = nn/n*100,
         prozent = round(prozent, 1)) %>% 
  arrange(desc(prozent)) %>% 
  filter(cuisine_description == "Latin (Cuban, Dominican, Puerto Rican, South & Central American)") 
```

---
# What is the output of this script?

```{r echo=FALSE}
df %>% 
  add_count(boro) %>% 
  add_count(cuisine_description, boro) %>% 
  select(cuisine_description, boro, n, nn) %>% 
  distinct() %>% 
  arrange(desc(nn)) %>% 
  mutate(prozent = nn/n*100,
         prozent = round(prozent, 1)) %>% 
  arrange(desc(prozent)) %>% 
  filter(cuisine_description == "Latin (Cuban, Dominican, Puerto Rican, South & Central American)") 
```

---
# References

```{r, results='asis', echo=FALSE}

print_bib_rmd(bib, start = 1, stop = 7)
```
