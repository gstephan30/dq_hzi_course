# Description of the Flare Dataset
[Link to Source](https://www.kaggle.com/flaredown/flaredown-autoimmune-symptom-tracker/home)

## Introduction
Flaredown is an app that helps patients of chronic autoimmune and invisible illnesses improve their symptoms by avoiding triggers and evaluating their treatments. Each day, patients track their symptom severity, treatments and doses, and any potential environmental triggers (foods, stress, allergens, etc) they encounter.

## About the data
nstead of coupling symptoms to a particular illness, Flaredown asks users to create their unique set of conditions, symptoms and treatments (“trackables”). They can then “check-in” each day and record the severity of symptoms and conditions, the doses of treatments, and “tag” the day with any unexpected environmental factors.

__User:__ includes an ID, age, sex, and country.

__Condition:__ an illness or diagnosis, for example Rheumatoid Arthritis, rated on a scale of 0 (not active) to 4 (extremely active).

__Symptom:__ self-explanatory, also rated on a 0–4 scale.

__Treatment:__ anything a patient uses to improve their symptoms, along with an optional dose, which is a string that describes how much they took during the day. For instance “3 x 5mg”.

__Tag:__ a string representing an environmental factor that does not occur every day, for example “ate dairy” or “rainy day”.

__Food:__ food items were seeded from the publicly-available USDA food database. Users have also added many food items manually.

__Weather:__ weather is pulled automatically for the user's postal code from the Dark Sky API. Weather parameters include a description, precipitation intensity, humidity, pressure, and min/max temperatures for the day.

If users do not see a symptom, treatment, tag, or food in our database (for instance “Abdominal Pain” as a symptom) they may add it by simply naming it. This means that the data requires some cleaning, but it is patient-centered and indicates their primary concerns.

## Suggested Questions

* Does X treatment affect Y symptom positively/negatively/not at all? What are the most strongly-correlated symptoms and treatments?
* Are there subsets within our current diagnoses that could more accurately represent symptoms and predict effective treatments?
* Can we reliably predict what triggers a flare for a given user or all users with a certain condition?
* Could we recommend treatments more effectively based on similarity of users, rather than specific symptoms and conditions? (Netflix recommendations for treatments)
* Can we quantify a patient’s level of disease activity based on their symptoms? How different is it from our existing measures?
* Can we predict which symptom should be treated to have the greatest effect on a given illness?
* How accurately can we guess a condition based on a user’s symptoms?
* Can we detect new interactions between treatments?


Please email logan@flaredown.com if you have questions about the project.